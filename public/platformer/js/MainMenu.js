console.log('hi');

MainMenu = {};

MainMenu.init = function () {
    this.keys = this.game.input.keyboard.addKeys({
        space: Phaser.KeyCode.SPACEBAR
    });
}

MainMenu.preload = function () {
    this.game.load.image('background', 'images/menu_bg.png');
    this.game.load.image('title', 'images/mementomoritext.png');
    this.game.load.image('press-space', 'images/pressspacetostart.png');
    this.game.load.audio('bgm', ['audio/bgm.mp3', 'audio/bgm.ogg']);
}

MainMenu.create = function () {
    this.game.add.image(0, 0, 'background');
    this.game.add.image(25, 25, 'title');
    this.game.add.image(25, 500, 'press-space');
    this.bgm = this.game.add.audio('bgm');
    this.bgm.loopFull();
}

MainMenu.update = function () {
    if (this.keys.space.isDown) {
        this.game.state.start('play', true, false, {level: 0});
    }
}

MainMenu.shutdown = function () {
    this.bgm.stop();
};