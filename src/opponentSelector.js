import React from "react";

export default class OpponentSelector extends React.Component {
    render() {
        return(
            <div>
                <h2 >Please select your opponent</h2>
                <div className={'portraitContainer'}>
                    <div onClick={() => this.props.onClick('Anatoliy')} className={'opponentCard'}>
                        <div className={'opponentName'}>
                            <h4>Anatoliy</h4>
                            <h4 className={'hard'}>HARD</h4>
                        </div>
                        <img className={'portrait'} src={"./Anatoliy.jpg"} />
                    </div>
                    <div onClick={() => this.props.onClick('Alex')} className={'opponentCard'}>
                        <div className={'opponentName'} >
                            <h4>Alex</h4>
                            <h4 className={'medium'}>MEDIUM</h4>
                        </div>
                        <img className={'portrait'} src={"./Alex.jpg"} />
                    </div>
                </div>
            </div>
        );
    }
}