import React from "react";

export default class OpponentInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            descriptionHidden: false,
        }
    }
    render() {
        return (
            <div className={'portraitContainer'}>
                <img onClick={() => this.handleClick()} src={this.props.opponent.image} className={'portrait'} alt={this.props.opponent.name}/>
                <div hidden={this.state.descriptionHidden}>
                    <h4 className={'opponentDescription'}>Opponent: {this.props.opponent.name}</h4>
                    <p>{this.props.opponent.description}</p>
                    <button onClick={() => this.props.changeOpponent()}>Change opponent</button>
                </div>
            </div>
        );
    }

    handleClick() {
        this.setState({
           descriptionHidden: !this.state.descriptionHidden,
        });
    }
}