export default class Move {
    constructor(targetLocation) {
        this.targetLocation = targetLocation;
        this.squares = [];
        this.score = null;
        this.nextPossibleMoves = [];
    }
}