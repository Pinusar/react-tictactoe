import React from "react";

export default class Footer extends React.Component {
    render() {
        return(
            <div>
                <p>Artwork author: Justin Nichol</p>
                <a href={'https://opengameart.org/content/flare-portrait-pack-number-five'}>Artwork source</a>
            </div>
        );
    }
}