import React from "react";
import "./index.css";
import Board from "./board";
import Ai from "./ai";

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [
                {
                    squares: Array(9).fill(null)
                },
            ],
            xIsNext: true,
            step: 0,
        }
    }

    render() {
        const history = this.state.history;
        const current = history[history.length - 1];
        const winner = calculateWinner(current.squares);
        const moves = history.map((step, move) => {
            const desc = move ?
                'Go to move #' + move :
                'Go to game start';
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        let status;
        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = (this.state.xIsNext ? 'Your turn' : "Opponent's turn");
        }

        return (
            <div className="game" onKeyDown={(event) => this.undoMove(event)}>
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }

    handleClick(i) {
        const history = this.state.history;
        const current = history[history.length - 1];
        if (current.squares[i] || calculateWinner(current.squares)) {
            return;
        }

        const newSquares = current.squares.slice();
        newSquares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{squares: newSquares}]),
            xIsNext: !this.state.xIsNext,
            step: history.length,
        });
    }

    undoMove(event) {
        if (event.key === 'z') {
            this.jumpTo(this.state.step - 2);
        }
    }

    jumpTo(step) {
        this.setState(
            {
                history: this.state.history.slice(0, step + 1),
                step: step,
                xIsNext: (step % 2 === 0),
            }
        );
    }
}

function calculateWinner(squares) {
    const winningLines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    for (let i = 0; i < winningLines.length; i++) {
        const a = winningLines[i][0];
        const b = winningLines[i][1];
        const c = winningLines[i][2];

        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }

    return null;
}

/*
1 - win
0 - draw
-1 - loss
 */
function evaluateBoard(squares, player) {
    const winner = calculateWinner(squares);
    if (winner) {
        if (winner === player) {
            return 1;
        } else {
            return -1;
        }
    } else {
        return 0;
    }
}

/*
Returns: array of integers(0-8) representig possible moves
 */
function getPossibleMoves(squares) {
    return  squares
        .map((value, index) => value === null ? index : '-')
        .filter((value) => value !== '-');
}


function isGameOver(squares) {
    if (!squares.includes(null) || calculateWinner(squares)) {
        return true;
    } else {
        return false;
    }
}

export {Game, calculateWinner, evaluateBoard, getPossibleMoves, isGameOver};