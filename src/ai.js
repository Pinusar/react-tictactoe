import {evaluateBoard, getPossibleMoves, isGameOver} from "./game";
import Move from "./move";

export default class Ai {
    constructor(player) {
        this.possibleBoardStates = [];
        this.currentMoveSequence = [];
        this.player = player;
        this.moveTree = {nextPossibleMoves: []};
        this.isMaximizing = true;
    }

    setPlayer(player) {
        this.player = player;
    }

    buildMoveTree(squares, player, startNode) {
        const possibleMoves = getPossibleMoves(squares);

        for (const targetSquare of possibleMoves) {
            const newSquares = squares.slice();
            newSquares[targetSquare] = player;

            const move = new Move(targetSquare);
            move.squares = newSquares;

            if (isGameOver(newSquares)) {
                move.score = evaluateBoard(newSquares, this.player);
                startNode.nextPossibleMoves.push(move)
            } else {
                startNode.nextPossibleMoves.push(move)
                const nextPlayer = (player === 'X') ? 'O' : 'X';
                this.buildMoveTree(newSquares, nextPlayer, move);
            }
        }
    }

    assignScores() {
        const possibleMoves = this.moveTree.nextPossibleMoves;
        for (const move of possibleMoves) {
            move.score = this.calculateMoveScore(move);
        }
    }

    calculateMoveScore(move) {
        if (move.nextPossibleMoves.length > 0) {
            this.isMaximizing = !this.isMaximizing;
            for (const nextMove of move.nextPossibleMoves) {
                nextMove.score = this.calculateMoveScore(nextMove);
            }
            if (this.isMaximizing) {
                move.nextPossibleMoves.sort((a, b) => b.score - a.score);
            } else {
                move.nextPossibleMoves.sort((a, b) => a.score - b.score);
            }
            this.isMaximizing = !this.isMaximizing;
            return move.nextPossibleMoves[0].score;
        } else {
            return move.score;
        }
    }

    findBestMove(board) {
        this.buildMoveTree(board, this.player, this.moveTree);
        this.assignScores();
        this.moveTree.nextPossibleMoves.sort((a, b) => b.score - a.score);
        return this.moveTree.nextPossibleMoves[0].targetLocation;
    }

    findRandomMove(board) {
        const possibleMoves = getPossibleMoves(board);
        return possibleMoves[Math.floor(Math.random() * possibleMoves.length)]
    }

    averageMove(board) {
        if (Math.random() > 0.4) {
            return this.findBestMove(board);
        } else {
            return this.findRandomMove(board)
        }
    }
}