import React from "react";
import "./index.css";

function Square(props) {
    return (
        <button
            className="square"
            onClick={() => props.onClick()}
        >
            {props.value}
        </button>
    );
}


export default class Board extends React.Component {
    renderSquare(i) {
        return <Square
            value={this.props.squares[i]}
            onClick={() => this.props.onClick(i)}
        />;
    }

    renderRow(squares) {
        return (
            <div className="board-row">
                {squares.map((value) => {
                    return this.renderSquare(value);
                })}
            </div>
        );
    }

    render() {
        const x = Array(8).fill(null);
        return (
            <div>
                {this.renderRow([0, 1, 2])}
                {this.renderRow([3, 4, 5])}
                {this.renderRow([6, 7, 8])}
            </div>
        );
    }
}