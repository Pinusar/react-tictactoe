import {
    calculateWinner,
    evaluateBoard,
    getPossibleMoves,
    findBestMove,
    isGameOver,
} from "./game";
import Ai from './ai';

it('calculates winner', () => {
    const drawBoard = ['X', 'O', 'X', null, null, null, null, null, null];
    const xWinsBoard = ['X', 'X', 'X', 'O', 'O', null, null, null, null];
    const oWinsBoard = ['X', 'X', null, 'O', 'O', 'O', null, null, 'X'];

    expect(calculateWinner(drawBoard)).toEqual(null);
    expect(calculateWinner(xWinsBoard)).toEqual('X');
    expect(calculateWinner(oWinsBoard)).toEqual('O');
});

it('evaluates board', () => {
    const drawBoard = ['X', 'O', 'X', null, null, null, null, null, null];
    const xWinsBoard = ['X', 'X', 'X', 'O', 'O', null, null, null, null];
    const oWinsBoard = ["X", "O", "X", "X", "O", "O", null, "O", "X"];

    expect(evaluateBoard(drawBoard, 'O')).toEqual(0);
    expect(evaluateBoard(xWinsBoard, 'X')).toEqual(1);
    expect(evaluateBoard(xWinsBoard, 'O')).toEqual(-1);
    expect(evaluateBoard(oWinsBoard, 'O')).toEqual(1);
});

it('gets possible moves', () => {
    const emptyBoard = Array(9).fill(null);
    const partiallyFilledBoard = ['X', 'O', 'X', null, null, null, null, null, null];
    const fullBoard = Array(9).fill('X');

    expect(getPossibleMoves(emptyBoard)).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8]);
    expect(getPossibleMoves(partiallyFilledBoard)).toEqual([3, 4, 5, 6, 7, 8]);
    expect(getPossibleMoves(fullBoard)).toEqual([]);
});

it('checks if a game is over', () => {
    const fullBoard = ['X', 'O', 'X', 'X', 'X', 'O', 'O', 'X', 'O'];
    const xWinsBoard = ['X', 'X', 'X', 'O', 'O', null, null, null, null];

    expect(isGameOver(fullBoard)).toEqual(true);
    expect(isGameOver(xWinsBoard)).toEqual(true);
});

it('builds move tree', function () {
    const board = ['X', 'O', 'X', null, null, 'O', null, 'O', 'X'];
    let ai = new Ai('X');

    expect(ai.findBestMove(board)).toEqual(4);

    const board3 = ['X', 'O', 'X', null, null, 'O', null, null, 'X'];
    ai = new Ai('O');

    expect(ai.findBestMove(board3)).toEqual(4);
});
