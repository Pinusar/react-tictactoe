import React from 'react';
import ReactDOM from 'react-dom';
import {Game} from './game';
import Title from "./title";
import SinglePlayerGame from "./singlePlayerGame";
import OpponentInfo from "./opponentInfo";
import Footer from "./footer";
import OpponentSelector from "./opponentSelector";
import GameRoom from "./gameRoom";

ReactDOM.render(
    <div style={{display: 'inline'}}>
        <Title/>
        <GameRoom />
        <Footer/>
        <a href={'./breakout.html'}>Play breakout</a>
        <br />
        <a href={'./platformer/index.html'}>Play platformer</a>
    </div>,
    document.getElementById('root')
);


