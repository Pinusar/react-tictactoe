export default class Opponent {
    constructor(name) {
        const descriptions = {
            "Anatoliy" : "Anatoliy is an experienced tic-tac-toe player who has participated in numerous tournaments.",
            "Alex" : "Alex is a traveller who enjoys a game of tic-tac-toe by the campfire with some wine.",
        }

        this.name = name;
        this.description = descriptions[name];
        this.image = "./" + this.name + ".jpg";
    }
}