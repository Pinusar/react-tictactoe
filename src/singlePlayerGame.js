import {calculateWinner, Game, isGameOver} from "./game";
import Ai from "./ai";

export default class SinglePlayerGame extends Game {

    handleClick(i) {
        this.playerMove(i);
    }

    playerMove(i) {
        const history = this.state.history;
        const current = history[history.length - 1];
        if (current.squares[i] || calculateWinner(current.squares)) {
            return;
        }

        const newSquares = current.squares.slice();
        newSquares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{squares: newSquares}]),
            xIsNext: !this.state.xIsNext,
            step: history.length,
        }, () => setTimeout(() => this.aiMove('O'), 1000));
    }

    aiMove(mark) {
        if (this.props.opponent === 'Anatoliy') {
            this.findBestMove(mark)
        } else if (this.props.opponent === 'Alex') {
            this.findAverageMove(mark);
        } else {
            console.log("Unexpected opponent!");
        }
    }

    findBestMove(mark) {
        const history = this.state.history;
        const current = history[history.length - 1];
        if (isGameOver(current.squares)) {
            return;
        }
        const newSquares = current.squares.slice();
        const ai = new Ai(mark);
        const targetSquare = ai.findBestMove(current.squares);
        newSquares[targetSquare] = mark;
        this.setState({
            history: history.concat([{squares: newSquares}]),
            xIsNext: !this.state.xIsNext,
            step: history.length,
        });
    }

    findAverageMove(mark) {
        const history = this.state.history;
        const current = history[history.length - 1];
        if (isGameOver(current.squares)) {
            return;
        }
        const newSquares = current.squares.slice();
        const ai = new Ai(mark);
        const targetSquare = ai.averageMove(current.squares);
        newSquares[targetSquare] = mark;
        this.setState({
            history: history.concat([{squares: newSquares}]),
            xIsNext: !this.state.xIsNext,
            step: history.length,
        });
    }
}