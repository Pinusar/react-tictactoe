import React from "react";
import OpponentSelector from "./opponentSelector";
import SinglePlayerGame from "./singlePlayerGame";
import OpponentInfo from "./opponentInfo";
import Opponent from "./opponent";

export default class GameRoom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showOpponentSelector: true,
            showOpponentInfo: false,
            showGame: false,
            opponent: new Opponent('Alex'),
        }
    }

    render() {
        return (
            <div>
                {this.state.showOpponentSelector ? <OpponentSelector onClick={(opponentName) => this.handleOpponentSelectorClick(opponentName)} /> : null}
                {this.state.showOpponentInfo ? <OpponentInfo opponent={this.state.opponent} changeOpponent={() => this.changeOpponent()}/> : null}
                {this.state.showGame ? <SinglePlayerGame opponent={this.state.opponent.name} /> : null}
            </div>
        );
    }

    handleOpponentSelectorClick(opponentName) {
        this.setState({
            showOpponentSelector: false,
            showOpponentInfo: true,
            showGame: true,
            opponent: new Opponent(opponentName),
        });
    }

    changeOpponent() {
        this.setState({
            showOpponentSelector: true,
            showOpponentInfo: false,
            showGame: false,
            opponent: new Opponent('Alex'),
        });
    }
}